const fastify = require('fastify')()
const path = require('path')
const db = require('./db')

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'dist')
})

fastify.get('/', (req, reply) => reply.sendFile('index.html'))

fastify.get('/db', async (request, reply) => {
  const allHosts = await db.allHosts()
  reply.send(allHosts)
})

fastify.post('/db', async (request, reply) => {
  const updatedHosts = db.addHost(request.body)
  reply.send(updatedHosts)
})

fastify.delete('/db', async (request, reply) => {
  const updatedHosts = await db.deleteHost(request.body)
  console.log(updatedHosts);
  reply.send(updatedHosts)
})

const start = async () => {
  try {
    await fastify.listen(3000, '0.0.0.0')
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

start()
