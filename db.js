const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('config/db.json')
const db = low(adapter)

db.defaults({ hosts: [] }).write()

const allHosts = () => db.get('hosts').value()
const addHost = ({ title, port }) => db.get('hosts').push({ title, port }).write()
const deleteHost = async ({ title }) => {
  await db.get('hosts').remove({ title }).write()
  return await db.get('hosts').value()
}

module.exports = {
  allHosts,
  addHost,
  deleteHost
}
