FROM mhart/alpine-node:11 as builder
WORKDIR /usr/src
COPY . .
RUN yarn
RUN yarn build

FROM mhart/alpine-node:11 as base
WORKDIR /usr/src
COPY --from=builder /usr/src/package.json /usr/src/yarn.lock /usr/src/
RUN yarn --production
COPY --from=builder /usr/src/ .

FROM mhart/alpine-node:base-11
WORKDIR /usr/src
ENV NODE_ENV="production"
COPY --from=base /usr/src .
EXPOSE 3000
VOLUME [ "/usr/src/config" ]
CMD ["node", "server.js"]
