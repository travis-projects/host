# Host

A super simple host to quickly access all project urls.

## Run

You can run with docker (recommended) or with something like PM2.

For Docker:

```bash
docker pull registry.gitlab.com/travis-projects/host:latest
# Change port to anything you like
# A volume is shared for a persistant db file = otherwise it will be lost if a container is deleted.
docker run --name=host -itd -p 80:3000 -v /config/data:/usr/src/config --restart=always host
```

Or PM2:

```bash
# Will run on port 3000
pm2 server.js
```

Now you can add hosts, to easily access running projects from a dashboard!
